#!/bin/bash

## Special variable $0 to $n, $*,$@, $#,$$, $?

## $0 -> Inside the script you can print name of the script.
echo $0

## $1 First argument which is parsed to the script

echo "First argument = " $1
echo "second argument = " $2
echo "Third argument = " $3
echo "Fourth argument = " $4

#  $* , $@will display all the arguments

echo $*
echo $@

# $# : Total no of argumnets
echo $#
