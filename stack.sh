#!/bin/bash

LOG=/tmp.stack.LOG
rm -f $LOG

## Check whether the script is excecuted as root user or not
USER_ID=$(id -u)
if [ $USER_ID -ne 0 ]; then
    echo "Please run using root user or with sudp command"
    exit 1
fi

echo -e "Install web server"
yum install httpd -y &>>$LOG

if [ $? -eq 0 ]; then 
    echo "Success"

else 
    echo "failed try again"
    echo "Check the log file :: $LOG for mor info"
    exit 1
fi